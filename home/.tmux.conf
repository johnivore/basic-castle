# to reload tmux config:
#   :source-file ~/.tmux.conf
# or
#   tmux source-file ~/.tmux.conf

set -g base-index 1

# update vars to share SSH agent
set -g update-environment "DISPLAY SSH_ASKPASS SSH_AGENT_PID SSH_CONNECTION WINDOWID XAUTHORITY"
set-environment -g SSH_AUTH_SOCK $HOME/.ssh/ssh-auth-sock

# set -g default-terminal tmux-256color
set -g default-terminal screen-256color

# don't clear screen on less/vi exit
set-window-option -g alternate-screen off

# increase scrollback buffer from the default of 2000
set -g history-limit 20000

# Window status
#set -g window-status-current-format ' #(pwd="#{pane_current_path}"; echo ${TMUX_TITLE_PREFIX}${pwd####*/}) '
#set -g window-status-format ' #(pwd="#{pane_current_path}"; echo ${TMUX_TITLE_PREFIX}${pwd####*/}) '
set -g window-status-current-format '⚫'
set -g window-status-format '⚫'

# disable status updates since the clock/hostname are no longer printed
set-option -g status-interval 0

# -- theme --

# NOTE: to print tmux colors:
# for i in {0..255}; do printf "\x1b[38;5;${i}mcolour%-5i\x1b[0m" $i ; if ! (( ($i + 1 ) % 8 )); then echo ; fi ; done

# Current window colors
set -g window-status-current-style bg=black,fg=colour46

# Window with activity status
# fg and bg are flipped here due to a bug in tmux
set -g window-status-activity-style bg=yellow,fg=black

# Status update interval
set -g status-interval 10

# Basic status bar colors
set -g status-fg colour34
set -g status-bg black

# Left side of status bar
set -g status-left-length 0
set -g status-left ''

# Right side of status bar
set-option -g status-right ""
#set -g status-right-style bg=black,fg=colour2
#set -g status-right-length 32
#set -g status-right "#h #[fg=colour34]%Y-%m-%d %Z #[fg=colour34]%H:%M"

# Pane border
set -g pane-border-style bg=default,fg=default

# Active pane border
set -g pane-active-border-style bg=default,fg=green

# Pane number indicator
set -g display-panes-colour default
set -g display-panes-active-colour default

# Clock mode
set -g clock-mode-style 24

# Message
set -g message-style bg=default,fg=default

# Command message
set -g message-command-style bg=default,fg=default

# Mode
set -g mode-style bg=blue,fg=default
# Window separator
set -g window-status-separator " "

# Window status alignment
set -g status-justify left


# -- keys --

# Set the prefix to ^A.
unbind C-b
set -g prefix C-a
bind C-a send-prefix

# vi-style keys
setw -g mode-keys vi

# make arrow keys behave properly
set-window-option -g xterm-keys on

# Bind appropriate commands similar to screen.
# lockscreen ^X x
unbind ^X
bind ^X lock-server
unbind x
bind x lock-server

# screen ^C c
unbind ^C
bind ^C new-window
unbind c
bind c new-window

# detach ^D d
unbind ^D
bind ^D detach

# displays *
unbind *
bind * list-clients

# next ^@ ^N sp n
unbind ^@
bind ^@ next-window
unbind ^N
bind ^N next-window
unbind " "
bind " " next-window
unbind n
bind n next-window

# title A
unbind A
bind A command-prompt "rename-window %%"

# windows ^W w
unbind ^W
bind ^W list-windows
unbind w
bind w list-windows

# quit \
unbind '\'
bind '\' confirm-before "kill-server"

# kill K k
unbind K
bind K confirm-before "kill-window"
unbind k
bind k confirm-before "kill-window"

# redisplay ^L l
unbind ^L
bind ^L refresh-client
unbind l
bind l refresh-client

unbind -
bind - split-window -v
unbind |
bind | split-window -h

# :kB: focus up
unbind Tab
bind Tab select-pane -t:.+
unbind BTab
bind BTab select-pane -t:.-

# " windowlist -b
# unbind '"'
# bind '"' choose-window

# set -g mode-mouse on

set -s escape-time 0

# come on tmux, use my default shell
set-option -g default-shell $SHELL


bind -n S-left  prev
bind -n S-right next
bind -n C-S-left  swap-window -d -t -1
bind -n C-S-right swap-window -d -t +1

# nested tmux!  see http://www.stahlke.org/dan/tmux-nested/ and buy that dude a beer
# (commenting out due to no longer using for now)
#bind -n M-F11 set -qg status-fg colour240
#bind -n M-F12 set -qg status-fg colour130
#bind -n S-up \
#    send-keys M-F12 \; \
#    set -qg status-fg colour240 \; \
#    unbind -n S-left \; \
#    unbind -n S-right \; \
#    unbind -n S-C-left \; \
#    unbind -n S-C-right \; \
#    set -qg prefix C-b
#bind -n S-down \
#    send-keys M-F11 \; \
#    set -qg status-fg colour130 \; \
#    bind -n S-left  prev \; \
#    bind -n S-right next \; \
#    bind -n C-S-left swap-window -t -1 \; \
#    bind -n C-S-right swap-window -t +1 \; \
#    set -qg prefix C-a
